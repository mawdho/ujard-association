<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encaissements', function (Blueprint $table) {
            $table->id();

            $table->string('reference')->unique();
            $table->string('type')->enum(['Cotisation','Sanction', 'Dons et Autres']);
            $table->double('montant');
            $table->string("libelle");
            $table->foreignIdFor(User::class);
            $table->integer('status')->enum([-1, 0, 1])->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encaissements');
    }
};
