<?php

use App\Models\Poste;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membres', function (Blueprint $table) {
            $table->id();

            $table->string('matricule')->unique();
            $table->string('photo')->default('profile.png');
            $table->string('nom');
            $table->string('prenom');
            $table->string('sexe');
            $table->date('date_naissance');
            $table->string('lieu_naissance');
            $table->string('profession');
            $table->string('filiation');
            $table->string('adresse');
            $table->string('telephone');
            $table->string('email');
            $table->string('numero_adhesion')->unique();
            $table->foreignIdFor(Poste::class);
            $table->integer('status')->enum([0, 1])->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membres');
    }
};
