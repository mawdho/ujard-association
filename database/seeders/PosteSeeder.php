<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PosteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('postes')->truncate();
        
        DB::table('postes')->insert([
            'reference' => 'Membre',
        ]);

        DB::table('postes')->insert([
            'reference' => 'President'
        ]);

        DB::table('postes')->insert([
            'reference' => 'Vice-President'
        ]);

        DB::table('postes')->insert([
            'reference' => 'Secretaire General'
        ]);

        DB::table('postes')->insert([
            'reference' => 'Tresorier'
        ]);

        // Caisse
        DB::table('caisses')->insert([
            'libelle' => 'Caisse de l\'association',
            'solde' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
