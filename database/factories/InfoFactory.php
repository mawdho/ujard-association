<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Info>
 */
class InfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nom' => "UNION DES JEUNES AMIS ET RESSORTISSANTS DE DJAFOUNABHEH",
            'slogan' => Str::upper('Union-Succès-Progrès'),
            'telephone' => $this->faker->phoneNumber(),
            'email' => $this->faker->safeEmail(),
            'adresse' => $this->faker->address(),
        ];
    }
}
