
<!-- Modal -->
<div class="modal fade" id="mouvement_details_modal-{{ $mouvement->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Détails du mouvement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mt-3">
                            <span>Date :</span>
                            <strong>{{ $mouvement->created_at->format('d-m-Y') }}</strong>
                        </h4>
                        <div class="card-title">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <span>Reference :</span>
                                    <strong>{{ $mouvement->reference }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Type :</span>
                                    <strong>{{ $mouvement->type }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Montant :</span>
                                    <strong>{{ number_format($mouvement->montant, 0, ',', ' ') }} GNF</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Libellé :</span>
                                    <strong>{{ $mouvement->description }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Initiateur :</span>
                                    <strong>{{ $mouvement->user->name }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Statut :</span>
                                    <strong>
                                        @if ($mouvement->status === -1)
                                            <span class="badge badge-danger">Annulé</span>
                                        @elseif ($mouvement->status === 0)
                                            <span class="badge badge-warning">En attente</span>
                                        @else
                                            <span class="badge badge-success">Validé</span>
                                        @endif
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                @if ($mouvement->status === 0)
                    <form action="{{ route('mouvements.annuler', $mouvement) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-warning btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir annuler cet mouvement ?')">
                            <i class="fa fa-exclamation-triangle mr-2"></i>
                            <strong>Annuler</strong>
                        </button>
                    </form>
                    <form action="{{ route('mouvements.valider', $mouvement) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-success btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir valider cet mouvement ?')">
                            <i class="fa fa-check mr-2"></i>
                            <strong>Valider</strong>
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>