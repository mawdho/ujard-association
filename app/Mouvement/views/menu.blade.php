<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-arrow-expand"></i><span class="hide-menu"><h3>Mouvements</h3> </span></a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('mouvements.create') }}" class="sidebar-link">
                <i class="mdi mdi-plus-circle"></i>
                <span class="hide-menu"> Ajouter un mouvement </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('mouvements.index') }}" class="sidebar-link">
                <i class="mdi mdi-format-list-numbers"></i>
                <span class="hide-menu"> Liste des mouvements </span>
            </a>
        </li>
    </ul>
</li>