
<div class="modal fade modal-md" id="mouvement_edit_modal-{{ $mouvement->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('mouvements.update', $mouvement) }}" method="POST">
            @csrf @method('PUT')
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modification de l'mouvement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="card mt-3">
                    <div class="card-body">
                
                            <div class="card-text row">
                
                                <div class="form-group col-12">
                                    <label for="type" class="control-label">Type :</label>
                                    <select name="type" class="form-control form-control-lg" id="type" required>
                                        <option selected disabled>Selectionnez le type</option>
                                        <option value="Debit" @if ($mouvement->type === 'Debit') selected @endif>
                                            Debit
                                        </option>
                                        <option value="Credit" @if ($mouvement->type === 'Credit') selected @endif>
                                            Credit
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="montant" class="control-label">Montant :</label>
                                    <input type="number" class="form-control form-control-lg" id="montant" name="montant" value="{{ old('montant') ?? $mouvement->montant }}" required>
                                </div>
                
                                <div class="form-group col-12">
                                    <label for="description" class="control-label">Description :</label>
                                    <textarea class="form-control form-control-lg" id="description" name="description" required>
                                        {{ old('description') ?? $mouvement->description }}
                                    </textarea>
                                </div>
                        </form>
                
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-rounded" data-dismiss="modal">
                    <i class="fa fa-window-close mr-2"></i>
                    <strong>Fermer</strong>
                </button>
                <button type="submit" class="btn btn-primary btn-rounded">
                    <i class="fa fa-save mr-2"></i>
                    <strong>Enregistrer</strong>
                </button>
            </div>
        </form>
        </div>
    </div>
</div>