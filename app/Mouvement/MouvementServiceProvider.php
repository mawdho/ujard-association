<?php

namespace App\Mouvement;

use Illuminate\Support\ServiceProvider;

class MouvementServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__. "/views", "Mouvement");
    }
}
