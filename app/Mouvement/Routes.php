<?php

use App\Mouvement\MouvementController;

Route::resource('mouvements', MouvementController::class);

// Annuler/Valider
Route::post('mouvements/{mouvements}/annuler', [MouvementController::class, 'annuler'])
    ->name('mouvements.annuler');

Route::post('mouvements/{mouvements}/valider', [MouvementController::class, 'valider'])
    ->name('mouvements.valider');