<?php

namespace App\Mouvement;

use App\Models\Caisse;
use App\Models\Mouvement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MouvementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soldeCaisse = Caisse::pluck('solde')->first();
        $mouvements  = Mouvement::with('user')->latest()->get();
        return view('Mouvement::list', compact('mouvements', 'soldeCaisse'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mouvement::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Mouvement::create([
            'reference' => now()->format('dmY.His'),
            'type' => $request->type,
            'montant' => $request->montant,
            'montant' => $request->montant,
            'description' => $request->description,
            'user_id' =>auth()->id(),
        ]);

        toastr()->info('Le mouvement a été initié avec succès !!!');
        return to_route('mouvements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mouvement  $mouvement
     * @return \Illuminate\Http\Response
     */
    public function show(Mouvement $mouvement)
    {
        if ($mouvement) {
            return view('Mouvement::details', compact('mouvement'));
        } else {
            toastr()->error('Le mouvement selectionné n\'existe pas !');
            return to_route('mouvements.index');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mouvement  $mouvement
     * @return \Illuminate\Http\Response
     */
    public function edit(Mouvement $mouvement)
    {
        if ($mouvement) {
            return view('Mouvement::edit', compact('mouvement'));
        } else {
            toastr()->error('Le mouvement selectionné n\'existe pas !');
            return to_route('mouvements.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mouvement  $mouvement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mouvement $mouvement)
    {
        $mouvement->update([
            'type' => $request->type,
            'montant' => $request->montant,
            'montant' => $request->montant,
            'description' => $request->description,
        ]);

        toastr()->info('Le mouvement a été initié avec succès !!!');
        return to_route('mouvements.index');
    }
    public function annuler(Mouvement $mouvement)
    {
        if ($mouvement) {
            $mouvement->status = -1;
            $mouvement->save();

            toastr()->info('Lemouvement a été annulé avec succès !!!');
            return to_route('mouvements.index');
        } else {
            toastr()->info('Lemouvement selectionné n\'existe pas !!!');
            return to_route('mouvements.index');
        }
        
    }
    
    public function valider(Mouvement $mouvement)
    {
        $caisse = Caisse::first();
        if ($mouvement) {
            if ($mouvement->type === 'Credit') {
                $mouvement->status = 1;
                $mouvement->save();
    
                $caisse->solde += $mouvement->montant;
                $caisse->save();
    
                toastr()->info('L\'encaissement a été validé avec succès !!!');
                return to_route('mouvements.index');
            } elseif ($mouvement->type === 'Debit' && $mouvement->montant <= $caisse->solde) {
                $mouvement->status = 1;
                $mouvement->save();
            
                $caisse->solde -= $mouvement->montant;
                $caisse->save();
            } else {
                toastr()->info('Le solde de la caisse est insuffisant !!!');
                return to_route('mouvements.index');
            }
            
        } else {
            toastr()->info('L\'encaissement selectionné n\'existe pas !!!');
            return to_route('mouvements.index');
        }
        
    }

}
