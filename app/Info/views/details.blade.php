@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Information de l'association</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Détails</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-md-4 mr-1">
            <div class="card border-primary  shadow">
                <div class="card-body px-4">
                    <div class="img-container">
                        <img src="{{ asset('storage/' . $info->logo) }}" class="img-fluid" height="75px">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card border-primary  shadow ">
                <div class="card-header bg-primary">
                    <h4 class="m-b-0 text-white text-center mb-2">{{ $info->nom }}</h4>
                </div>
                <div class="card-body px-4">
                    <h3 class="card-title font-italic">{{ $info->slogan }}</h3>
                    <div class="card-text row">
                        <div class="col-12">
                            <p>
                                <i class="mdi mdi-phone-classic mr-2"></i>
                                <span class="font-bold">{{ $info->telephone }}</span>
                            </p>
                            <p>
                                <i class="mdi mdi-map-marker-multiple mr-2"></i>
                                <span class="font-bold">{{ $info->adresse }}</span>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <i class="mdi mdi-email mr-2"></i>
                            <span class="font-bold">{{ $info->email }}</span>
                        </div>
                    </div>
                    
                    <div class="mt-3 mb-2">
                        <button type="button" class="btn btn-primary btn-rounded mr-auto" data-toggle="modal" data-target="#info-edit-modal" data-whatever="@mdo">
                            <i class="mdi mdi-table-edit mr-2"></i>
                            Modifier
                        </button>
                        @include('Info::edit')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection