@extends('layouts.app')

@section('content')

    @if ($info->count() > 0)
        @include('Info::details')
    @else    
        @include('Info::create')
    @endif

@endsection