<div id="info-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modification des infos de l'association</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('infos.update', $info) }}" method="POST" enctype="multipart/form-data">
                    @csrf @method('PUT')
                    <div class="row">

                        <div class="form-group col-12">
                            <label for="logo" class="control-label">Logo :</label>
                            <input type="file" class="form-control form-control-lg @error('logo') is-nvalid @enderror" id="logo" name="logo">
                            @error('logo')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-12">
                            <label for="nom" class="control-label">Nom :</label>
                            <input type="text" class="form-control form-control-lg" id="nom" name="nom" value="{{ old('nom') ?? $info->nom }}">
                        </div>

                        <div class="form-group col-12">
                            <label for="slogan" class="control-label">Slogan :</label>
                            <input type="text" class="form-control form-control-lg" id="slogan" name="slogan" value="{{ old('slogan') ?? $info->slogan }}">
                        </div>

                        <div class="form-group col-12">
                            <label for="telephone" class="control-label">Téléphone :</label>
                            <input type="text" class="form-control form-control-lg" id="telephone" name="telephone" value="{{ old('telephone') ?? $info->telephone }}">
                        </div>

                        <div class="form-group col-12">
                            <label for="email" class="control-label">Email :</label>
                            <input type="email" class="form-control form-control-lg" id="email" name="email" value="{{ old('email') ?? $info->email }}">
                        </div>

                        <div class="form-group col-12">
                            <label for="adresse" class="control-label">Adresse :</label>
                            <input type="text" class="form-control form-control-lg" id="adresse" name="adresse" value="{{ old('adresse') ?? $info->adresse }}">
                        </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-info waves-effect waves-light">
                    <i class="fa fa-save mr-2"></i>
                    Enregistrer les modifs
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
