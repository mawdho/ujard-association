@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Qjout des informations de l'association</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ajout</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

    <form action="{{ route('infos.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">

            <div class="form-group col-md-6">
                <label for="logo" class="control-label">Logo :</label>
                <input type="file" class="form-control form-control-lg" id="logo" name="logo">
            </div>

            <div class="form-group col-md-6">
                <label for="nom" class="control-label">Nom :</label>
                <input type="text" class="form-control form-control-lg" id="nom" name="nom" >
            </div>

            <div class="form-group col-md-6">
                <label for="slogan" class="control-label">Slogan :</label>
                <input type="text" class="form-control form-control-lg" id="slogan" name="slogan" >
            </div>

            <div class="form-group col-md-6">
                <label for="telephone" class="control-label">Téléphone :</label>
                <input type="text" class="form-control form-control-lg" id="telephone" name="telephone" >
            </div>

            <div class="form-group col-md-6">
                <label for="email" class="control-label">Email :</label>
                <input type="email" class="form-control form-control-lg" id="email" name="email">
            </div>

            <div class="form-group col-md-6">
                <label for="adresse" class="control-label">Adresse :</label>
                <input type="text" class="form-control form-control-lg" id="adresse" name="adresse" >
            </div>

            <div class="form-group col-md-6">
                <label for="reglement" class="control-label">Règlement :</label>
                <input type="file" class="form-control form-control-lg" id="reglement" name="reglement" >
            </div>

        </div>

        <div class="">
            <button type="submit" class="btn btn-danger waves-effect waves-light">
                <i class="fa fa-save mr-2"></i>
                Enregistrer
            </button>
        </div>
    </form>

@endsection