<li class="sidebar-item">
    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('infos.index') }}" aria-expanded="false">
        <i class="mdi mdi-information-outline"></i>
        <span class="hide-menu"><h3>Information</h3></span>
    </a>
</li>