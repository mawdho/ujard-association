<?php

use App\Info\InfoController;

Route::resource('infos', InfoController::class);