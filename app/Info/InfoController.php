<?php

namespace App\Info;

use App\Http\Controllers\Controller;
use App\Models\Info;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = Info::first();
        return view('Info::index', compact('info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Info::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $info = Info::create([
            'nom' => $request->nom,
            'slogan' => $request->slogan,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'adresse' => $request->adresse,
        ]);

        if ($request->logo) {

            $this->validate($request, [
                'logo' => 'required|image|mimes:png,jpg,jpeg,gif'
            ]);

            $info->logo = $this->uploadImage($request->logo);
            $info->save();
        }

        toastr()->success('L\'info a été ajoutée avec succès !!!');
        return redirect()->route('infos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Info  $info
     * @return \Illuminate\Http\Response
     */
    public function show(Info $info)
    {
        if ($info) {
            return view('Info::show', compact('info'));
        } else {
            toastr()->info('L\'info selectionnée n\'existe pas !');
            return back();
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Info  $info
     * @return \Illuminate\Http\Response
     */
    public function edit(Info $info)
    {
        if ($info) {
            return view('Info::edit', compact('info'));
        } else {
            toastr()->info('L\'info selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Info  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Info $info)
    {
        $info->update([
            'nom' => $request->nom,
            'slogan' => $request->slogan,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'adresse' => $request->adresse,
        ]);

        if ($request->logo) {

            $this->validate($request, [
                'logo' => 'required|image|mimes:png,jpg,jpeg,gif'
            ]);

            $info->logo = $this->uploadImage($request->logo);
            $info->save();
        }

        toastr()->success('L\'info a été modifiée avec succès !!!');
        return redirect()->route('infos.index');
    }
}
