<?php

namespace App\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('indexLogin', 'storeLogin');
    }

    public function indexLogin()
    {
        return view('Auth::login');
    }

    public function storeLogin(Request $request)
    {
        // $this->validate($request, [
        //     'pseudo' => 'required|exists:users,pseudo',
        //     'password' => 'required'
        // ]);

        $credentials = $request->only(['pseudo', 'password']);

        if (auth()->attempt(array_merge($credentials, ['status' => 1]))) {
            toastr()->success('Authentification reussie.  BIENVENUE !'); 
            return to_route('infos.index');
        } else {
            return back()->with('erreur', 'Login ou mot de pqss incorrect !');
        }
    }

    public function logout()
    {
        session()->flush();
        Auth::logout();

        toastr()->success('Deconnexion reussie.  AU REVOIR !');
        return to_route('auth.login');
    }

}
