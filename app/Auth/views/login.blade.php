<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="{{ asset('dist/css/style.min.css') }}" rel="stylesheet">
</head>
<body style="background-color: rgb(207, 200, 200)">

    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" ">
        <div class="auth-box">
            <div id="loginform">
                <div class="logo">
                    <span class="db">
                        <img src="{{ asset('assets/images/logo-icon.png') }}" alt="logo" />
                    </span>
                    <h5 class="font-medium m-b-20">Se connecter</h5>
                </div>
                <!-- Form -->
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        @if (session()->has('erreur'))
                            <div>{{ session()->flash('erreur') }}</div>
                        @endif
                    </div>
                    <div class="col-12">
                        <form class="form-horizontal m-t-20"  action="{{ route('auth.login') }}" method="POST">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ti-user"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-lg" placeholder="Pseudo" name="pseudo">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                                </div>
                                <input type="password" class="form-control form-control-lg" placeholder="Mot de passe" name="password">
                            </div>
                            <div class="form-group text-center">
                                <div class="col-xs-12 p-b-20">
                                    <button class="btn btn-block btn-lg btn-info" type="submit">
                                        Connexion
                                    </button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

</body>
</html>
