<?php

use App\Auth\AuthController;


// Login
Route::get('login', [AuthController::class, 'indexLogin'])->name('auth.login');
Route::post('login', [AuthController::class, 'storeLogin']);

// Register
Route::get('register', [AuthController::class, 'indexRegister'])->name('auth.register');
Route::post('register', [AuthController::class, 'storeRegister']);

// Logout
Route::get('logout', [AuthController::class, 'logout'])->name('auth.logout');