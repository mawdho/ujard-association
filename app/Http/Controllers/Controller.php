<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Ramsey\Uuid\Uuid;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function uploadImage($file)
    {
        $imageName = Uuid::uuid4()->toString();
        Image::make($file)->resize(450, 450)->save('storage/'.$imageName);
        return $imageName;
    }
}
