<?php

namespace App\Caisse;

use Illuminate\Support\ServiceProvider;

class CaisseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadViewsFrom(__DIR__. "/views", "Membre");
    }
}
