<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-money-bill-alt"></i><span class="hide-menu"><h3>Dépenses</h3></span></a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('depenses.create') }}" class="sidebar-link">
                <i class="mdi mdi-plus-circle"></i>
                <span class="hide-menu"> Ajouter une dépense </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('depenses.index') }}" class="sidebar-link">
                <i class="mdi mdi-format-list-numbers"></i>
                <span class="hide-menu"> Liste des dépenses </span>
            </a>
        </li>
    </ul>
</li>