@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Dépenses</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ajout d'une dépense</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3 row">
    <div class="card-body col-md-8 offset-md-2 shadow border">

        <form action="{{ route('depenses.store') }}" method="POST">
            @csrf
            <div class="card-text row">
                
                <div class="form-group col-6">
                    <label for="libelle" class="control-label">Libellé :</label>
                    <input type="text" class="form-control form-control-lg" id="libelle" name="libelle" value="{{ old('libelle') }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="montant" class="control-label">Montant :</label>
                    <input type="number" class="form-control form-control-lg" id="montant" name="montant" value="{{ old('montant') }}" required>
                </div>

                <div class="form-group col-md-6 offset-md-3 mt-4">
                    <button type="submit" class="btn btn-primary btn-rounded w-50">
                        <i class="fa fa-save mr-2"></i>
                        Emnregistrer
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection
