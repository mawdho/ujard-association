
<!-- Modal -->
<div class="modal fade" id="depense_details_modal-{{ $depense->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Détails de la dépense</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mt-3">
                            <span>Date :</span>
                            <strong>{{ $depense->created_at->format('d-m-Y') }}</strong>
                        </h4>
                        <div class="card-title">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <span>Reference :</span>
                                    <strong>{{ $depense->reference }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Montant :</span>
                                    <strong>{{ number_format($depense->montant, 0, ',', ' ') }} GNF</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Libellé :</span>
                                    <strong>{{ $depense->libelle }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Initiateur :</span>
                                    <strong>{{ $depense->user->name }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Statut :</span>
                                    <strong>
                                        @if ($depense->status === -1)
                                            <span class="badge badge-danger">Annulé</span>
                                        @elseif ($depense->status === 0)
                                            <span class="badge badge-warning">En attente</span>
                                        @else
                                            <span class="badge badge-success">Validé</span>
                                        @endif
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                @if ($depense->status === 0)
                    <form action="{{ route('depenses.annuler', $depense) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-warning btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir annuler cette dépense ?')">
                            <i class="fa fa-exclamation-triangle mr-2"></i>
                            <strong>Annuler</strong>
                        </button>
                    </form>
                    <form action="{{ route('depenses.valider', $depense) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-success btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir valider cette dépense ?')">
                            <i class="fa fa-check mr-2"></i>
                            <strong>Valider</strong>
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>