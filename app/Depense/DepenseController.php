<?php

namespace App\Depense;

use App\Models\Caisse;
use App\Models\Depense;
use App\Models\Mouvement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depenses = Depense::with('user')->latest()->get();
        return view('Depense::list', compact('depenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Depense::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Depense::create([
            'reference' => now()->format('dmY.His'),
            'libelle' => $request->libelle,
            'montant' => $request->montant,
            'user_id' => auth()->id(),
        ]);
        toastr()->info('La dépense a été initié avec succès !!!');
        return to_route('depenses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function show(Depense $depense)
    {
        if ($depense) {
            return view('Depense::show', compact('depense'));
        } else {
        toastr()->error('La dépense selectionnée n\'existe pas !!!');
        return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function edit(Depense $depense)
    {
        if ($depense) {
            return view('Depense::edit', compact('depense'));
        } else {
        toastr()->error('La dépense selectionnée n\'existe pas !!!');
        return back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Depense $depense)
    {
        if ($depense) {
            $depense->update([
                'libelle' => $request->libelle,
                'montant' => $request->montant,
            ]);
            toastr()->info('La dépense a été modifiée avec succès !!!');
            return to_route('depenses.index');
        } else {
        toastr()->error('La dépense selectionnée n\'existe pas !!!');
        return back();
        }
        
    }

    public function annuler(Depense $depense)
    {
        if ($depense) {
            $depense->status = -1;
            $depense->save();

            toastr()->info('La depense a été annulée avec succès !!!');
            return to_route('depenses.index');
        } else {
            toastr()->error('Lq depense selectionnée n\'existe pas !!!');
            return to_route('depenses.index');
        }
        
    }
    
    public function valider(Depense $depense)
    {
        $caisse = Caisse::first();
        if ($depense) {
            if ($depense->montant <= $caisse->solde) {
                $depense->status = 1;
                $depense->save();
    
                Mouvement::create([
                    'reference' => now()->format('dmY.His'),
                    'type' => 'Debit',
                    'montant' => $depense->montant,
                    'description' => $depense->reference .'/'. 
                                    $depense->created_at->format('d-m-Y H:i') .'/'. 
                                    $depense->libelle,
                    'user_id' => auth()->id(),
                    'status' => 1,
                ]);
    
                $caisse->solde -= $depense->montant;
                $caisse->save();
    
                toastr()->info('La dépense a été validée avec succès !!!');
                return to_route('depenses.index');
            } else {
                toastr()->error('Le solde de la caisse est insuffisant !!!');
                return to_route('depenses.index');
            }
        } else {
            toastr()->error('La dépense selectionné n\'existe pas !!!');
            return to_route('depenses.index');
        }
        
    }
}
