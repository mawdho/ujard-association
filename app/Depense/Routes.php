<?php

use App\Depense\DepenseController;

Route::resource('depenses', DepenseController::class);

// Annuler/Valider
Route::post('depenses/{depense}/annuler', [DepenseController::class, 'annuler'])
    ->name('depenses.annuler');

Route::post('depenses/{depense}/valider', [DepenseController::class, 'valider'])
    ->name('depenses.valider');