<?php

use App\Dashboard\DashboardController;

Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');

// Générer un bulletin d'adhesion
Route::get('membres/generate-bulletin', [DashboardController::class, 'generateBulletin'])->name('membres.bulletin');