<?php

namespace App\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Caisse;
use App\Models\Membre;
use Barryvdh\DomPDF\Facade\Pdf;

class DashboardController extends Controller
{
    public function index()
    {
        $data['soldeCaisse'] = Caisse::pluck('solde')->first();

        $membres = Membre::all();
        $data['nombreMembres'] = $membres->count();
        $data['membresActifs'] = $membres->where('status', 1)->count();
        $data['membresSuspendus'] = $membres->where('status', '!==', 1)->count();

        return view('Dashboard::index', compact('data'));
    }

    public function generateBulletin()
    {
        $pdf = PDF::loadView('Dashboard::bulletin-pdf')->setPaper('A4', 'Landscape');
        return $pdf->stream('bulletin.pdf'); 
    }
}
