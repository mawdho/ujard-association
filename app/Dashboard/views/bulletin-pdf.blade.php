<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bulletin d'adhesion</title>
    <style>
        body {
            
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 2px solid black;
            padding: 5px
        }
        tr td {
            padding: 30px;
        }
        .entete {
            display: inline-flex;
            widows: 100%;
        }
        .text-body {
            text-align: justify;
        }

    </style>
</head>
<body>
    <div class="bulletinAdhesion">
        <div class="entete">
            <div class="leftSide">
                <h4>République de Guinée</h4>
                <small>Travail-Justice-Soldarité</small>
            </div>
            <div class="rightSide">
                <h3>Bulletin d'adhesion --U.J.A.R.D--</h3>
            </div>
        </div>
        <h5>Siege social: DAR-ES-SALAM II, commune de Ratoma</h5>
        <div class="table">
            <table>
                <thead>
                    <tr>
                        <th rowspan="2">Prénoms et Nom</th>
                        <th colspan="2">Naissance</th>
                        <th colspan="2">Filiation</th>
                        <th rowspan="2">Ville</th>
                        <th rowspan="2">Téléphone</th>
                        <th rowspan="2">Email</th>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <th>Lieu</th>
                        <th>Père</th>
                        <th>Mère</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
            <div class="text-body">
                <p>
                    Je déclare par la présente souhaiter devenir membre de cette associatio: "U.J.A.R.D".
                    A ce titre, Je déclare reconnaître l'objet de l'association et en avoir accepté les status ainsi que le règk=lement interieur qui sont mis à ma disposition. J'ai pris bonne note des droits et des devoirs des membres de l'association et j'accepte de verser ma cotisation régulièrement. <br>
                    Je fournis pour mon inscriptionles documents demandés suivants: <br>
                    <ul>
                        <li>
                            Photocopie de la carte d'identité nationale ou extrait de naissance; 
                        </li>
                        <li>
                            4 photos d'identités;
                        </li>
                    </ul>
                </p>

            </div>
            <div class="text-footer">
                <span>Fait à Conakry le {{ now()->format('d/m/Y') }}</span>
                <div class="signature">
                    <div>
                        <h5>Le tuteur: </h5>
                        Mme/Mlle/M. .........................
                        .....................................
                    </div>
                    <div>
                        <h5>Le President de l'association</h5>
                        Mme/Mlle/M. ............................
                        ........................................
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>