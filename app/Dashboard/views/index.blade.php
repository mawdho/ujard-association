@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Tableau de bord</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">

            <!-- ============================================================== -->
            <!-- Info Box -->
            <!-- ============================================================== -->
            <div class="card-body border-top">
                <div class="row m-b-0">
                    <!-- col -->
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex align-items-center">
                            <div class="m-r-10"><span class="text-orange display-5"><i class="mdi mdi-wallet"></i></span></div>
                            <div><span>Solde de la caisse</span>
                                <h3 class="font-medium m-b-0 text-center">{{ number_format($data['soldeCaisse'], 0, ',', ' ') }} GNF</h3>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex align-items-center">
                            <div class="m-r-10"><span class="text-cyan display-5"><i class="fa fa-users"></i></span></div>
                            <div><span>Nombre de membres</span>
                                <h3 class="font-medium m-b-0 text-center">{{ $data['nombreMembres'] }}</h3>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex align-items-center">
                            <div class="m-r-10"><span class="text-success display-5"><i class="fa fa-users"></i></span></div>
                            <div><span>Membres Actifs</span>
                                <h3 class="font-medium m-b-0 text-center">{{ $data['membresActifs'] }}</h3></div>
                        </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex align-items-center">
                            <div class="m-r-10"><span class="text-danger display-5"><i class="fa fa-users"></i></span></div>
                            <div><span>Membres Suspendus</span>
                                <h3 class="font-medium m-b-0 text-center">{{ $data['membresSuspendus'] }}</h3>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body border-top">
                <div class="row m-b-0">
                    <!-- col -->
                    <div class="col-12">
                        <div class="d-flex align-items-center">
                            <h4>
                                <span>Générer le canevas d'un bulletin d'adhesion </span>
                                <a href="{{ route('membres.bulletin') }}" class="btn btn-info btn-rounded ml-2">
                                    <i class="fa fa-cloud-download-alt mr-2"></i>
                                    <span>Générer</span>
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="text-center text-muted disabled">
        <img src="{{ asset('dist/css/icons/flag-icon-css/flags/gn.svg') }}" alt="drapeau national" class="">
    </div>
</div>

@endsection