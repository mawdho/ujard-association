<?php

namespace App\Membre;

use App\Models\Poste;
use App\Models\Membre;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade\Pdf;

class MembreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $membres = Membre::latest()->get();
        return view('Membre::list', compact('membres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $postes = Poste::where('status', 1)->get();
        return view('Membre::create', compact('postes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membre = Membre::create([
            'matricule' => Str::random(6),
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'sexe' => $request->sexe,
            'date_naissance' => $request->date_naissance,
            'lieu_naissance' => $request->lieu_naissance,
            'profession' => $request->profession,
            'filiation' => $request->filiation,
            'adresse' => $request->adresse,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'poste_id' => $request->poste_id,
            'numero_adhesion' => uniqid(now()->year)
        ]);

        if ($request->photo) {

            $this->validate($request, [
                'photo' => 'required|image|mimes:png,jpg,jpeg,gif',
            ]);

            $membre->photo = $this->uploadImage($request->photo);
            $membre->save();
        }

        toastr()->info('Le membre a été ajouté avec succès !!!');
        return to_route('membres.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Membre  $membre
     * @return \Illuminate\Http\Response
     */
    public function show(Membre $membre)
    {
        if ($membre) {
            $postes = Poste::where('status', 1)->get();
            return view('Membre::details', compact('membre', 'postes'));
        } else {
            toastr()->error('Le membre selectionné n\'existe pas!');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Membre  $membre
     * @return \Illuminate\Http\Response
     */
    public function edit(Membre $membre)
    {
        if ($membre) {
            $postes = Poste::where('status', 1)->get();
            return view('Membre::edit', compact('membre', 'postes'));
        } else {
            toastr()->error('Le membre selectionné n\'existe pas!');
            return back();
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Membre  $membre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membre $membre)
    {
        if ($membre) {
            $membre->update([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'sexe' => $request->sexe,
                'date_naissance' => $request->date_naissance,
                'lieu_naissance' => $request->lieu_naissance,
                'profession' => $request->profession,
                'filiation' => $request->filiation,
                'adresse' => $request->adresse,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'poste_id' => $request->poste_id,
            ]);

            $this->validate($request, [
                'photo' => 'required|image|mimes:png,jpg,jpeg,gif',
            ]);

            if ($request->photo) {
                $membre->photo = $this->uploadImage($request->photo);
                $membre->save();
            }
    
            toastr()->info('Le membre a été modifié avec succès !!!');
            return redirect()->route('membres.show', compact('membre'));
        } else {
            toastr()->error('Le membre selectionné n\'existe pas!');
            return back();
        }
    }

    public function suspendre(Membre $membre)
    {
        if ($membre) {
            $membre->status = 0;
            $membre->save();

            toastr()->info('Le membre a été suspendu avec succès !!!');
            return back();
        } else {
             toastr()->error('Le membre selectionné n\'existe pas !');
             return back();           
        }
    }
    
    public function activer(Membre $membre)
    {
        if ($membre) {
            $membre->status = 1;
            $membre->save();

            toastr()->info('Le membre a été activé avec succès !!!');
            return back();
        } else {
             toastr()->error('Le membre selectionné n\'existe pas !');
             return back();           
        }
    }

    /**
     * Permet d'exporter en PDF la liste des membres
     *
     * @return void
     */
    public function exportPDF()
    {
        $membres = Membre::latest()->get();
        $pdf = PDF::loadView('Membre::list-pdf', compact('membres'))->setPaper('A4', 'landscape');
        return $pdf->stream('membres.pdf');
    }

    public function printRecu(Membre $membre)
    {
        $president = Poste::where('reference', 'President')->with('membres')->first();
        $president1 = $president->membres->first();
        $pdf = PDF::loadView('Membre::recu-pdf', compact('membre', 'president1'));
        return $pdf->stream('reçu-' . Str::slug($membre->prenom) .'-'. Str::slug($membre->nom) . '.pdf');   
    }
}
