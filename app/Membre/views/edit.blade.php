

<div class="card mt-3 row">
    <div class="card-body col-12 shadow border">

        <form action="{{ route('membres.update', $membre) }}" method="POST" enctype="multipart/form-data">
            @csrf @method('PUT')
            <div class="card-text row">

                <div class="form-group col-md-4">
                    <label for="nom" class="control-label">Nom :</label>
                    <input type="text" class="form-control form-control-lg" id="nom" name="nom" value="{{ old('nom') ?? $membre->nom }}" required>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="prenom" class="control-label">Prénom :</label>
                    <input type="text" class="form-control form-control-lg" id="prenom" name="prenom" value="{{ old('prenom') ?? $membre->prenom }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="nom" class="control-label">Sexe :</label>
                    <select name="sexe" class="form-control form-control-lg" id="nom" required>
                        <option selected disabled>Selectionnez le sexe</option>
                        <option value="Masculin" @if ($membre->sexe === 'Masculin') selected @endif>Masculin</option>
                        <option value="Feminin" @if ($membre->sexe === 'Feminin') selected @endif>Feminin</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label for="date_naissance" class="control-label">Date de naissance</label>
                    <input type="date" class="form-control form-control-lg" id="date_naissance" name="date_naissance" value="{{ old('date_naissance') ?? date('Y-m-d', strtotime($membre->date_naissance)) }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="lieu_naissance" class="control-label">Lieu de naissance :</label>
                    <input type="text" class="form-control form-control-lg" id="lieu_naissance" name="lieu_naissance" value="{{ old('lieu_naissance') ?? $membre->lieu_naissance }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="profession" class="control-label">Profession</label>
                    <input type="text" class="form-control form-control-lg" id="profession" name="profession" value="{{ old('profession') ?? $membre->profession }}" required>
                </div>

                <div class="form-group col-md-8">
                    <label for="filiation" class="control-label">Filiation :</label>
                    <input type="text" class="form-control form-control-lg" id="filiation" name="filiation" value="{{ old('filiation') ?? $membre->filiation }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="adresse" class="control-label">Adresse</label>
                    <input type="text" class="form-control form-control-lg" id="adresse" name="adresse" value="{{ old('adresse') ?? $membre->adresse }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="telephone" class="control-label">Téléphone</label>
                    <input type="text" class="form-control form-control-lg" id="telephone" name="telephone" value="{{ old('telephone') ?? $membre->telephone }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="email" class="control-label">Email :</label>
                    <input type="email" class="form-control form-control-lg" id="email" name="email" value="{{ old('email') ?? $membre->email }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="photo" class="control-label">Photo :</label>
                    <input type="file" class="form-control form-control-lg" id="photo" name="photo">
                </div>
                
                <div class="form-group col-md-6">
                    <label for="poste_id" class="control-label">Poste :</label>
                    <select class="form-control form-control-lg custom-select" id="poste_id" name="poste_id">
                        @foreach ($postes as $poste)
                            <option value="{{ $poste->id }}" @if ($membre->poste->id === $poste->id) selected @endif>
                                {{ $poste->reference }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-6 mt-4">
                    <button type="submit" class="btn btn-primary btn-rounded w-50">
                        <i class="fa fa-save mr-2"></i>
                        Emnregistrer
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
