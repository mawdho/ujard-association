@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Membre</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ajout de membre</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3 row">
    <div class="card-body col-md-10 offset-md-1 shadow border">

        <form action="{{ route('membres.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-text row">

                <div class="form-group col-md-4">
                    <label for="nom" class="control-label">Nom :</label>
                    <input type="text" class="form-control form-control-lg" id="nom" name="nom" value="{{ old('nom') }}" required>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="prenom" class="control-label">Prénom :</label>
                    <input type="text" class="form-control form-control-lg" id="prenom" name="prenom" value="{{ old('prenom') }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="nom" class="control-label">Sexe :</label>
                    <select name="sexe" class="form-control form-control-lg" id="nom" required>
                        <option selected disabled>Selectionnez le sexe</option>
                        <option value="Masculin">Masculin</option>
                        <option value="Feminin">Feminin</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label for="date_naissance" class="control-label">Date de naissance</label>
                    <input type="date" class="form-control form-control-lg" id="date_naissance" name="date_naissance" value="{{ old('date_naissance') }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="lieu_naissance" class="control-label">Lieu de naissance :</label>
                    <input type="text" class="form-control form-control-lg" id="lieu_naissance" name="lieu_naissance" value="{{ old('lieu_naissance') }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="profession" class="control-label">Profession</label>
                    <input type="text" class="form-control form-control-lg" id="profession" name="profession" value="{{ old('profession') }}" required>
                </div>

                <div class="form-group col-md-8">
                    <label for="filiation" class="control-label">Filiation :</label>
                    <input type="text" class="form-control form-control-lg" id="filiation" name="filiation" value="{{ old('filiation') }}" required>
                </div>

                <div class="form-group col-md-4">
                    <label for="adresse" class="control-label">Adresse</label>
                    <input type="text" class="form-control form-control-lg" id="adresse" name="adresse" value="{{ old('adresse') }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="telephone" class="control-label">Téléphone</label>
                    <input type="text" class="form-control form-control-lg" id="telephone" name="telephone" value="{{ old('telephone') }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="email" class="control-label">Email :</label>
                    <input type="email" class="form-control form-control-lg" id="email" name="email" value="{{ old('email') }}" required>
                </div>

                <div class="form-group col-md-6">
                    <label for="photo" class="control-label">Photo :</label>
                    <input type="file" class="form-control form-control-lg" id="photo" name="photo">
                </div>
                
                <div class="form-group col-md-6">
                    <label for="poste_id" class="control-label">Poste :</label>
                    <select class="form-control form-control-lg custom-select" id="poste_id" name="poste_id">
                        @foreach ($postes as $poste)
                            <option value="{{ $poste->id }}">{{ $poste->reference }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-6 mt-4">
                    <button type="submit" class="btn btn-primary btn-rounded w-50">
                        <i class="fa fa-save mr-2"></i>
                        Emnregistrer
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection
