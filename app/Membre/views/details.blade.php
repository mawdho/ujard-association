@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Membre <strong>{{ $membre->nom }}</strong></h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile du membre</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <center class="m-t-30"> <img src="{{ asset('storage/' . $membre->photo) }}" class="rounded-circle mb-2" width="150" />
                        <h4 class="card-title m-t-10">{{ $membre->prenom .' '.$membre->nom }}</h4>
                        <h6 class="card-subtitle">{{ Str::upper($membre->numero_adhesion) }}</h6>
                    </center>
                </div>
                <div>
                    <hr> </div>
                <div class="card-body">
                    <small class="text-muted">Adresse Email</small>
                    <h6>{{ $membre->email }}</h6>
                    <small class="text-muted p-t-30 db">Téléphone</small>
                    <h6>{{ $membre->telephone }}</h6>
                    <small class="text-muted p-t-30 db">Adresse</small>
                    <h6>{{ $membre->adresse }}</h6>

                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Informations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Modifier les infos</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">

                    <div class="tab-pane fade show active" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Nom complet</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->prenom .' '. $membre->nom }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Sexe</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->sexe }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Date de naissance</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->date_naissance->format('d M Y') }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <strong>Lieu de naissance</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->lieu_naissance }}</p>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Filiation</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->filiation }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Profession</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->profession }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Poste</strong>
                                    <br>
                                    <p class="text-muted">{{ $membre->poste->reference }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    <strong>Statut</strong>
                                    <br>
                                    <p class="text-muted">
                                        @if ($membre->status === 0)
                                            <span class="badge badge-danger">Inactif</span>
                                        @else
                                            <span class="badge badge-success">Actif</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3 col-xs-6 b-r"></div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    @if ($membre->status === 0)
                                        <form action="{{ route('membres.activer', $membre) }}" method="post">
                                            @csrf
                                            <button 
                                                type="submit" 
                                                class="btn btn-success btn-rounded"
                                                onclick="return confirm('Êtes-vous sûr de vouloir activer ce membre ?')">
                                                <i class="fa fa-check mr-2"></i>
                                                Activer
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('membres.suspendre', $membre) }}" method="post" >
                                            @csrf
                                            <button 
                                                type="submit" 
                                                class="btn btn-danger btn-rounded"
                                                onclick="return confirm('Êtes-vous sûr de vouloir suspendre ce membre ?')">
                                                <i class="fa fa-ban mr-2"></i>
                                                Suspendre
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                            <div class="float-right pb-3">
                                <a href="{{ route('membres.recu', $membre) }}" class="btn btn-info btn-rounded mt-4">
                                    <i class="fa fa-file mr-2"></i>
                                    Imprimer le reçu
                                </a>
                                <a href="{{ route('membres.bulletin', $membre) }}" class="btn btn-secondary btn-rounded mt-4">
                                    <i class="fa fa-file mr-2"></i>
                                    Imprimer le bulletin d'adhesion
                                </a>
                            </div>
                        </div>
                    </div>
                        <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                            @include('Membre::edit')
                        </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>

@endsection
