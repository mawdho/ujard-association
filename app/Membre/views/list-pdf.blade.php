<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des membres</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 12pt;
        }
        table {
            table-layout: fixed;
            width: 100%;
            border-collapse: collapse;
            border: 3px solid black;
            text-align: left;
            vertical-align: middle;
        }
        thead {
            background-color: #333333;
            color: white;
        }
        thead th:nth-child(1) {
            width: 5%;
        }
        thead th:nth-child(4) {
            width: 9%;
        }
        thead th:nth-child(6) {
            width: 25%;
        }
        thead th:nth-child(7) {
            width: 7%;
        }
        tbody tr:nth-child(even) {
            background-color: #ddd;
        }
        th, td {
            border: 2px solid black;
            padding: 20px;
        }
    </style>
</head>
<body>
    <h2>Listes des membres</h2>
    <table style="width: 100%">
        <thead>
            <th style="font-weight: bold">#</th>
            <th style="font-weight: bold">Nom</th>
            <th style="font-weight: bold">Prenom</th>
            <th style="font-weight: bold">Sexe</th>
            <th style="font-weight: bold">Téléphone</th>
            <th style="font-weight: bold">Email</th>
            <th style="font-weight: bold">Statut</th>
        </thead>
        <tbody>
            @foreach ($membres as $membre)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $membre->nom }}</td>
                    <td>{{ $membre->prenom }}</td>
                    <td>{{ $membre->sexe }}</td>
                    <td>{{ $membre->telephone }}</td>
                    <td>{{ $membre->email }}</td>
                    <td>
                        @if ($membre->status === 0)
                            <span class="">Inactif</span>
                        @else
                            <span class="">Actif</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>