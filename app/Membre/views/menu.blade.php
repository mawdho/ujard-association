<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-people-carry"></i><span class="hide-menu"><h3>Membres</h3> </span></a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('membres.create') }}" class="sidebar-link">
                <i class="mdi mdi-plus-circle"></i>
                <span class="hide-menu"> Ajouter un membre </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('membres.index') }}" class="sidebar-link">
                <i class="mdi mdi-format-list-numbers"></i>
                <span class="hide-menu"> Liste des membres </span>
            </a>
        </li>
    </ul>
</li>