<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reçu d'adhesion</title>
    <style>
        body {
            display: flex;
            align-items: center;
        }
        .recu {
            background-color: skyblue;
            width: 500px;
            height: 280px;
            padding: 15px;
            padding-bottom: 10px;
            text-align: justify;
            font-size: 12pt;
            border: 4px solid gray;
            margin: auto;
        }
        h1 {
            font-weight: bold;
            text-transform: uppercase;
            text-decoration: underline;
            text-align: center;
        }
        .dateLieu {
            text-align: center;
        }
        .president {
            text-align: right;
        }
    </style>
</head>
<body>
    <div class="recu">
        <h1>Reçu d'adhesion</h1>
        <p>
            Je sousigné Mme/Mlle/M. <strong>{{ $president1->prenom .' '. $president1->nom }}</strong>, président(e) de la dite association, déclare avoir reçu le bulletin d'adhesion de <strong>{{ $membre->prenom . ' '. $membre->nom }}</strong> ainsi que l'ensemble des documents demandés.
            L'adhesion du membre sous-nommé est ainsi validée. Ce reçu confirme la qualité du membre postulant, et ouvre droit à la participation à l'assemblée générale de l'association.
        </p>
        <p class="dateLieu">Fait à Conakry le {{ now()->format('d/m/Y') }}<br>
            Le/La Président(e) <br> 
            Mme/Mlle/M. <strong>{{ $president1->prenom .' '. $president1->nom }}</strong>
        </p>
    </div>
</body>
</html>