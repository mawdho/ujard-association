<?php

namespace App\Membre;

use Illuminate\Support\ServiceProvider;

class MembreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__. "/views", "Membre");
    }
}
