<?php

use App\Membre\MembreController;

// Exporter la liste des membres en PDF
Route::get('membres/exportPDF', [MembreController::class, 'exportPDF'])->name('membres.exportPDF');

// Imprimer le reçu d'adhesion
Route::get('membres/{membre}/recu', [MembreController::class, 'printRecu'])->name('membres.recu');

Route::resource('membres', MembreController::class);

// Suspendre/Activer
Route::post('membres/{membre}/suspendre', [MembreController::class, 'suspendre'])->name('membres.suspendre');
Route::post('membres/{membre}/activer', [MembreController::class, 'activer'])->name('membres.activer');


