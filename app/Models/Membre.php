<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Membre extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'date_naissance' => 'date'
    ];

    /**
     * Get the poste that owns the Membre
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poste(): BelongsTo
    {
        return $this->belongsTo(Poste::class);
    }

}
