<?php

namespace App\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->get();
        return view('User::list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'pseudo' => 'required|unique:users,pseudo',
            'password' => 'required|confirmed',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'pseudo' => $request->pseudo,
            'password' => Hash::make($request->password)
        ]);

        toastr()->info('L\'utilisateur a été ajouté avec succès !!!');
        return to_route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if ($user) {
            return view('User::details', compact('user'));
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return to_route('users.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if ($user) {
            return view('User::edit', compact('user'));
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return to_route('users.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'pseudo' => 'required',
        ]);

        if ($user) {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'pseudo' => $request->pseudo,
                'password' => Hash::make($request->password)
            ]);
    
            toastr()->info('L\'utilisateur a été modifié avec succès !!!');
            return to_route('users.index');
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return to_route('users.index');
        }
    }

    public function suspendre(User $user)
    {
        if ($user) {
            $user->status = 0;
            $user->save();

            toastr()->info('L\'utilisateur a été suspendu avec succès !!!');
            return back();
        } else {
             toastr()->error('L\'utilisateur selectionné n\'existe pas !');
             return back();           
        }
    }
    
    public function activer(User $user)
    {
        if ($user) {
            $user->status = 1;
            $user->save();

            toastr()->info('L\'utilisateur a été activé avec succès !!!');
            return back();
        } else {
             toastr()->error('L\'utilisateur selectionné n\'existe pas !');
             return back();           
        }
    }

    public function changerMotPasse(Request $request, User $user)
    {
        if ($user) {

            $this->validate($request, [
                'current_password' => 'required',
                'new_password' => 'required|confirmed',
            ]);
 
            if (Hash::check($request->current_password, $user->password))
            {
                $user->password = Hash::make($request->current_password);
                $user->save();

                toastr()->info('Le mot de passe a été modifié avec succès !!!');
                return back();  
            } else {
                toastr()->error('L\'actuel mot de passe est incorrect !');
                return back();  
            }

        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return back();  
        }
        
    }

}
