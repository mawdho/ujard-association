<?php

use App\User\UserController;

Route::resource('users', UserController::class);

// Suspendre/Activer
Route::post('users/{user}/suspendre', [UserController::class, 'suspendre'])->name('users.suspendre');
Route::post('users/{user}/activer', [UserController::class, 'activer'])->name('users.activer');

// Changer le mot de passe
Route::post('users/{user}/changer-mot-passe', [UserController::class, 'changerMotPasse'])->name('users.changer-mot-passe');