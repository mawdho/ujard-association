<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu"><h3>Utilisateurs</h3> </span></a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('users.create') }}" class="sidebar-link">
                <i class="mdi mdi-plus-circle"></i>
                <span class="hide-menu"> Ajouter un utilisateur </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('users.index') }}" class="sidebar-link">
                <i class="mdi mdi-format-list-numbers"></i>
                <span class="hide-menu"> Liste des utilisateurs </span>
            </a>
        </li>
    </ul>
</li>