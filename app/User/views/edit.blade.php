
<div class="card mt-3 row">
    <div class="card-body col-md-10 offset-md-1 shadow border">

        <form action="{{ route('users.update', $user) }}" method="POST" >
            @csrf @method('PATCH')
            <div class="card-text row">

                <div class="form-group col-12">
                    <label for="name" class="control-label">Nom Complet :</label>
                    <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') ?? $user->name }}" required>
                    @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form-group col-12">
                    <label for="email" class="control-label">Email :</label>
                    <input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') ?? $user->email }}" required>
                    @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-12">
                    <label for="pseudo" class="control-label">Pseudo</label>
                    <input type="text" class="form-control form-control-lg @error('pseudo') is-invalid @enderror" id="pseudo" name="pseudo" value="{{ old('pseudo') ?? $user->pseudo }}" required>
                    @error('pseudo')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-12 mt-4">
                    <button type="submit" class="btn btn-primary btn-rounded w-50">
                        <i class="fa fa-save mr-2"></i>
                        Emnregistrer
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
