@extends('layouts.app')

@section('content')

<div class="page-breadcrumb">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Utilisateur <strong>{{ $user->name }}</strong></h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile de l'utilisateur</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <h3 class="mb-3">Changez le mot de passe</h3>
                    <form action="{{ route('users.changer-mot-passe', $user) }}" method="post">
                        @csrf 
                        <div class="row">
                            <div class="form-group col-12">
                                <label for="current_password">Actuel mot de passe</label>
                                <input type="password" name="current_password" id="current_password" class="form-control form-control-lg @error('current_password') is-invalid @enderror">
                                @error('current_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-12">
                                <label for="new_password">Nouveau mot de passe</label>
                                <input type="password" name="new_password" id="new_password" class="form-control form-control-lg @error('new_password') is-invalid @enderror">
                                @error('new_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-12">
                                <label for="new_password_confirmation">Confirmer nouveau mot de passe</label>
                                <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control form-control-lg">
                            </div>
                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-success btn-rounded w-100">
                                    <i class="fa fa-check mr-2"></i>
                                    <span>Enregistrer</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Informations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Modifier les infos</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">

                    <div class="tab-pane fade show active" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <div class="row mb-3 px-5">
                                <div class="col-md-6 b-r">
                                    <span>Nom complet :</span><br>
                                    <strong>{{ $user->name }}</strong>
                                </div>
                                <div class="col-md-6 b-r">
                                    <span>Email :</span><br>
                                    <strong>{{ $user->email }}</strong>
                                </div>
                            </div>
                            <div class="row mb-2 b-r px-5">
                                <div class="col-md-6 b-r">
                                    <span>Pseudo :</span><br>   
                                    <strong>{{ $user->pseudo }}</strong>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-6 b-r"></div>
                                <div class="col-md-3 col-xs-6 b-r">
                                    @if ($user->status === 0)
                                        <form action="{{ route('users.activer', $user) }}" method="post">
                                            @csrf
                                            <button 
                                                type="submit" 
                                                class="btn btn-success btn-rounded"
                                                onclick="return confirm('Êtes-vous sûr de vouloir activer cet utilisateur ?')">
                                                <i class="fa fa-check mr-2"></i>
                                                Activer
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('users.suspendre', $user) }}" method="post">
                                            @csrf
                                            <button 
                                                type="submit" 
                                                class="btn btn-danger btn-rounded"
                                                onclick="return confirm('Êtes-vous sûr de vouloir suspendre cet utilisateur ?')">
                                                <i class="fa fa-ban mr-2"></i>
                                                Suspendre
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                            @include('User::edit')
                        </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>

@endsection
