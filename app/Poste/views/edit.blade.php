<div class="modal fade" id="modal-add-poste-{{ $poste->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Modification du poste</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{ route('postes.update', $poste) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="modal-body row">

                    <div class="form-group col-12">
                        <label for="reference">Reference</label>
                        <input id="reference" class="form-control form-control-lg w-100 bg-light" type="text" name="reference" value="{{ old('reference') ?? $poste->reference }}">
                    </div>
                    
                    <div class="form-group col-12">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control form-control-lg w-100 bg-light">{{ old('description') ?? $poste->description }}</textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Fermer
                    </button>
                    <button type="submit" class="btn btn-info">
                        <i class="fa fa-check mr-2"></i>
                        Valider
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
