<li class="sidebar-item">
    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('postes.index') }}" aria-expanded="false">
        <i class="fa fa-briefcase"></i>
        <span class="hide-menu"><h3>Postes</h3></span>
    </a>
</li>
