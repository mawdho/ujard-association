@extends('layouts.app')

@section('styles')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb mb-3">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Liste des postes</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des postes</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-10 offset-lg-1">
        <div class="card shadow">
            <div class="card-body mt-3">
                <div class="float-right mb-3">
                    <button type="button" class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#modal-add-poste" data-whatever="@mdo">
                        <i class="fa fa-plus mr-2"></i>
                        Ajouter un poste
                    </button>
                    @include('Poste::create')
                </div>
                <div class="table-responsive">
                    <table id="lang_opt" class="table table-striped table-bordered display">
                        <thead >
                            <tr class="text-center">
                                <th style="font-weight: bold">#</th>
                                <th style="font-weight: bold">Reference</th>
                                <th style="font-weight: bold">Description</th>
                                <th style="font-weight: bold">Statut</th>
                                <th style="font-weight: bold">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postes as $poste)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $poste->reference }}</td>
                                    <td>{{ $poste->decription }}</td>
                                    <td class="text-center">
                                        @if ($poste->status === 0)
                                            <span class="badge badge-danger px-3 py-1">Inactif</span>
                                        @else
                                            <span class="badge badge-success px-3 py-1">Actif</span>
                                        @endif
                                    </td>
                                    <td class="float-right">
                                        <button type="button" class="btn btn-primary btn-sm btn-rounded" data-toggle="modal" data-target="#modal-add-poste-{{ $poste->id }}" data-whatever="@mdo">
                                            <i class="fa fa-edit mr-2"></i>
                                            <span>Editer le poste</span>
                                        </button>
                                        @include('Poste::edit')

                                        @if ($poste->status === 1)
                                            <form action="{{ route('postes.suspendre', $poste) }}" method="post" class="d-inline">
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir suspendre ce poste ?')">
                                                    <i class="fa fa-ban mr-2"></i>
                                                    <span>Suspendre</span>
                                                </button>
                                            </form>
                                        @else
                                        <form action="{{ route('postes.activer', $poste) }}" method="post" class="d-inline">
                                            @csrf
                                            <button type="submit" class="btn btn-success btn-sm btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir activer ce poste ?')">
                                                <i class="fa fa-check mr-2"></i>
                                                <span>Activer</span>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="../../dist/js/pages/datatable/datatable-basic.init.js" src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>

@endsection
