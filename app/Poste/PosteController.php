<?php

namespace App\Poste;

use App\Http\Controllers\Controller;
use App\Models\Poste;
use Illuminate\Http\Request;

class PosteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postes = Poste::latest()->get();
        return view('Poste::list', compact('postes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Poste::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reference'=> ['required', 'unique:postes,reference']
        ]);

        Poste::create([
            'Reference' => $request->reference,
            'description' => $request->description
        ]);

        toastr()->info('Le poste a été ajouté avec succès !!!');
        return redirect()->route('postes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poste  $poste
     * @return \Illuminate\Http\Response
     */
    public function show(Poste $poste)
    {
        if ($poste) {
            return view('Poste::details', compact('poste'));
        } else {
            toastr()->error('Le poste selectionné n\'existe pas !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Poste  $poste
     * @return \Illuminate\Http\Response
     */
    public function edit(Poste $poste)
    {
        if ($poste) {
            return view('Poste::edit', compact('poste'));
        } else {
            toastr()->error('Le poste selectionné n\'existe pas !');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Poste  $poste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poste $poste)
    {
        if ($poste) {
            $poste->update([
                'Reference' => $request->reference,
                'description' => $request->description
            ]);
    
            toastr()->info('Le poste a été modifié avec succès !!!');
            return redirect()->route('postes.index'); 
        } else {
            toastr()->error('Le poste selectionné n\'existe pas !');
            return back();
        }
        


    }

    public function suspendre(Poste $poste)
    {
        if ($poste) {
            $poste->status = 0;
            $poste->save();

            toastr()->info('Le poste a été suspendu avec succès !!!');
            return redirect()->route('postes.index');
        } else {
            toastr()->error('Le poste selectionné n\'existe pas!');
            return back();
        }
    }

    public function activer(Poste $poste)
    {
        if ($poste) {
            $poste->status = 1;
            $poste->save();

            toastr()->info('Le poste a été activé avec succès !!!');
            return redirect()->route('postes.index');
        } else {
            toastr()->error('Le poste selectionné n\'existe pas!');
            return back();
        }
    }
}
