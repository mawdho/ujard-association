<?php

namespace App\Poste;

use Illuminate\Support\ServiceProvider;

class PosteServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__. "/views", "Poste");
    }
}
