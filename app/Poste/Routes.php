<?php

use App\Poste\PosteController;

Route::resource('postes', PosteController::class);

// Activer/Suspendre un poste

Route::post('postes/suspendre/{poste}', [PosteController::class, 'suspendre'])
        ->name('postes.suspendre');

Route::post('postes/activer/{poste}', [PosteController::class, 'activer'])
        ->name('postes.activer');
