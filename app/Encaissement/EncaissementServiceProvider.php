<?php

namespace App\Encaissement;

use Illuminate\Support\ServiceProvider;

class EncaissementServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__. "/views", "Encaissement");
    }
}
