<?php

use App\Encaissement\EncaissementController;

Route::resource('encaissements', EncaissementController::class);

// Annuler/Valider
Route::post('encaissements/{encaissement}/annuler', [EncaissementController::class, 'annuler'])
    ->name('encaissements.annuler');

Route::post('encaissements/{encaissement}/valider', [EncaissementController::class, 'valider'])
    ->name('encaissements.valider');