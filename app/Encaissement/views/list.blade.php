@extends('layouts.app')

@section('styles')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb mb-3">
    <div class="row px-2">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Encaissement</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Liste des encaissements</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <div class="m-r-10">
                    {{-- <div class="lastmonth"></div> --}}
                </div>
                <div class=""><small>Date du jour</small>
                    <h4 class="text-info m-b-0 font-medium">{{ now()->translatedFormat('l jS F Y H:i') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card shadow">
            <div class="card-body mt-3">
                <div class="float-right mb-3">
                    <a href="{{ route('encaissements.create') }}" class="btn btn-primary btn-rounded">
                        <i class="fa fa-plus mr-2"></i>
                        Ajouter un encaissement
                    </a>
                </div>
                <div class="table-responsive">
                    <table id="lang_opt" class="table table-striped table-bordered display" style="width:100%">
                        <thead >
                            <tr >
                                <th style="font-weight: bold">#</th>
                                <th style="font-weight: bold">Date</th>
                                <th style="font-weight: bold">Reference</th>
                                <th style="font-weight: bold">Type</th>
                                <th style="font-weight: bold">Montant</th>
                                <th style="font-weight: bold">Statut</th>
                                <th style="font-weight: bold">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($encaissements as $encaissement)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $encaissement->created_at->format('d/m/Y H:i') }}</td>
                                    <td>{{ $encaissement->reference }}</td>
                                    <td>{{ $encaissement->type }}</td>
                                    <td>{{ number_format($encaissement->montant, 0, ',', ' ') }}</td>
                                    <td>
                                        @if ($encaissement->status === -1)
                                            <span class="badge badge-danger">Annulé</span>
                                        @elseif ($encaissement->status === 0)
                                            <span class="badge badge-warning">En attente</span>
                                        @else
                                            <span class="badge badge-success">Validé</span>
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-rounded btn-sm mr-2" data-toggle="modal" data-target="#encaissemet_details_modal-{{ $encaissement->id }}" data-whatever="@mdo">
                                            <i class="mdi mdi-eye mr-1"></i>
                                            Afficher
                                        </button>
                                        @include('Encaissement::details')
                                        @if ($encaissement->status === 0)
                                            <button type="button" class="btn btn-info btn-rounded btn-sm" data-toggle="modal" data-target="#encaissemet_edit_modal-{{ $encaissement->id }}" data-whatever="@mdo">
                                                <i class="mdi mdi-table-edit mr-1"></i>
                                                Editer
                                            </button>
                                            @include('Encaissement::edit')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="../../dist/js/pages/datatable/datatable-basic.init.js" src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>

@endsection
