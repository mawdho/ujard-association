
<!-- Modal -->
<div class="modal fade" id="encaissemet_details_modal-{{ $encaissement->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Détails e l'encaissement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mt-3">
                            <span>Date :</span>
                            <strong>{{ $encaissement->created_at->format('d-m-Y') }}</strong>
                        </h4>
                        <div class="card-title">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <span>Reference :</span>
                                    <strong>{{ $encaissement->reference }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Type :</span>
                                    <strong>{{ $encaissement->type }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Montant :</span>
                                    <strong>{{ number_format($encaissement->montant, 0, ',', ' ') }} GNF</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Libellé :</span>
                                    <strong>{{ $encaissement->libelle }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Initiateur :</span>
                                    <strong>{{ $encaissement->user->name }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <span>Statut :</span>
                                    <strong>
                                        @if ($encaissement->status === -1)
                                            <span class="badge badge-danger">Annulé</span>
                                        @elseif ($encaissement->status === 0)
                                            <span class="badge badge-warning">En attente</span>
                                        @else
                                            <span class="badge badge-success">Validé</span>
                                        @endif
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                @if ($encaissement->status === 0)
                    <form action="{{ route('encaissements.annuler', $encaissement) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-warning btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir annuler cet encaissement ?')">
                            <i class="fa fa-exclamation-triangle mr-2"></i>
                            <strong>Annuler</strong>
                        </button>
                    </form>
                    <form action="{{ route('encaissements.valider', $encaissement) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-success btn-rounded" onclick="return confirm('Êtes-vous sûr de vouloir valider cet encaissement ?')">
                            <i class="fa fa-check mr-2"></i>
                            <strong>Valider</strong>
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>