
<div class="modal fade modal-md" id="encaissemet_edit_modal-{{ $encaissement->id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('encaissements.update', $encaissement) }}" method="POST">
            @csrf @method('PUT')
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modification de l'encaissement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="card mt-3">
                    <div class="card-body">
                
                            <div class="card-text row">
                
                                <div class="form-group col-12">
                                    <label for="type" class="control-label">Type :</label>
                                    <select name="type" class="form-control form-control-lg" id="type" required>
                                        <option selected disabled>Selectionnez le type</option>
                                        <option value="Cotisation" @if ($encaissement->type === 'Cotisation') selected @endif>
                                            Cotisation
                                        </option>
                                        <option value="Sanction" @if ($encaissement->type === 'Sanction') selected @endif>
                                            Sanction
                                        </option>
                                        <option value="Dons et Autres" @if ($encaissement->type === 'Dons et Autres') selected @endif>
                                            Dons et Autres
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="montant" class="control-label">Montant :</label>
                                    <input type="number" class="form-control form-control-lg" id="montant" name="montant" value="{{ old('montant') ?? $encaissement->montant }}" required>
                                </div>
                
                                <div class="form-group col-12">
                                    <label for="libelle" class="control-label">Libellé :</label>
                                    <input type="text" class="form-control form-control-lg" id="libelle" name="libelle" value="{{ old('libelle') ?? $encaissement->libelle }}" required>
                                </div>
                        </form>
                
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-rounded" data-dismiss="modal">
                    <i class="fa fa-window-close mr-2"></i>
                    <strong>Fermer</strong>
                </button>
                <button type="submit" class="btn btn-primary btn-rounded">
                    <i class="fa fa-save mr-2"></i>
                    <strong>Enregistrer</strong>
                </button>
            </div>
        </form>
        </div>
    </div>
</div>