<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-wallet"></i><span class="hide-menu"><h3>Encaissements</h3> </span></a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('encaissements.create') }}" class="sidebar-link">
                <i class="mdi mdi-plus-circle"></i>
                <span class="hide-menu"> Ajouter un encaissement </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('encaissements.index') }}" class="sidebar-link">
                <i class="mdi mdi-format-list-numbers"></i>
                <span class="hide-menu"> Historiques des encaissements </span>
            </a>
        </li>
    </ul>
</li>