<?php

namespace App\Encaissement;

use App\Http\Controllers\Controller;
use App\Models\Caisse;
use App\Models\Encaissement;
use App\Models\Mouvement;
use Illuminate\Http\Request;

class EncaissementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encaissements = Encaissement::with('user')->latest()->get();
        return view('Encaissement::list', compact('encaissements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Encaissement::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $encaissement = Encaissement::create([
            'reference' => now()->format('dmY.His'),
            'type' => $request->type,
            'montant' => $request->montant,
            'libelle' => $request->libelle,
            'user_id' => auth()->id(),
        ]);

        toastr()->info('L\'encaissement a été initié avec succès !!!');
        return to_route('encaissements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Encaissement  $encaissement
     * @return \Illuminate\Http\Response
     */
    public function show(Encaissement $encaissement)
    {
        return view('Encaissement::details', compact('encaissement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Encaissement  $encaissement
     * @return \Illuminate\Http\Response
     */
    public function edit(Encaissement $encaissement)
    {
        if ($encaissement) {
            return view('Encaissement::edit', compact('encaissement'));
        } else {
            toastr()->error('L\'encaissement n\'existe pas !');
            return to_route('encaissements.index');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Encaissement  $encaissement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Encaissement $encaissement)
    {
        $encaissement->update([
            'type' => $request->type,
            'montant' => $request->montant,
            'libelle' => $request->libelle,
        ]);

        toastr()->info('L\'encaissement a été modifié avec succès !!!');
        return to_route('encaissements.index');
    }

    public function annuler(Encaissement $encaissement)
    {
        if ($encaissement) {
            $encaissement->status = -1;
            $encaissement->save();

            toastr()->info('L\'encaissement a été annulé avec succès !!!');
            return to_route('encaissements.index');
        } else {
            toastr()->error('L\'encaissement selectionné n\'existe pas !!!');
            return to_route('encaissements.index');
        }
        
    }
    
    public function valider(Encaissement $encaissement)
    {
        if ($encaissement) {
            $encaissement->status = 1;
            $encaissement->save();

            Mouvement::create([
                'reference' => now()->format('dmY.His'),
                'type' => 'Credit',
                'montant' => $encaissement->montant,
                'description' => $encaissement->reference .'/'. 
                                $encaissement->created_at->format('d-m-Y H:i') .'/'. 
                                $encaissement->libelle,
                'user_id' => auth()->id(),
                'status' => 1,
            ]);

            $caisse = Caisse::first();
            $caisse->solde += $encaissement->montant;
            $caisse->save();

            toastr()->info('L\'encaissement a été validé avec succès !!!');
            return to_route('encaissements.index');
        } else {
            toastr()->error('L\'encaissement selectionné n\'existe pas !!!');
            return to_route('encaissements.index');
        }
        
    }
}
