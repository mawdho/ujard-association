<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {

    require_once base_path("app/Dashboard/Routes.php");


    require_once base_path("app/Info/Routes.php");
    require_once base_path("app/Poste/Routes.php");
    require_once base_path("app/Membre/Routes.php");
    require_once base_path("app/Encaissement/Routes.php");
    require_once base_path("app/Depense/Routes.php");
    require_once base_path("app/Mouvement/Routes.php");
    require_once base_path("app/User/Routes.php");
});
require_once base_path("app/Auth/Routes.php");

