<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
<!-- Custom CSS -->
<link href="{{ asset('assets/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/extra-libs/c3/c3.min.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link  href="{{ asset('dist/css/style.min.css') }}" rel="stylesheet">