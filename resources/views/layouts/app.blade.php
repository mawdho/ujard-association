<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('partials.styles')
    @yield('styles')
    <title>UJARD</title>
    @toastr_css
</head>
<body>
  <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper">

      @include('partials.sidebar')

      @include('partials.header')

      <div class="page-wrapper">

        @yield('content')

      </div>

    </div>

    <div class="chat-windows"></div>
    @include('partials.scripts')
    @yield('scripts')
    
    @toastr_js
    @toastr_render
</body>
</html>
